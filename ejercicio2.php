<?php
// autocarga de clases
spl_autoload_register(function ($clase) {
    require $clase . '.php';
});

use clases\ejercicio2\Cuenta;
use clases\ejercicio2\Persona;

$cuenta1=new Cuenta();

$cuenta1->recibirAbonos(1000);
$cuenta1->pagarRecibos(250);

echo $cuenta1->getSaldo();

$persona1=new Persona();

$persona1->setDni("20211818q");
$persona1->setNombre("Pepe");
var_dump($persona1);
$persona1->agregarCuenta(new Cuenta());
$persona1->agregarCuenta(new Cuenta());
$persona1->agregarTelefono("625625625");
$persona1->agregarTelefono("626625625");
$persona1->getCuentas()[0]->pagarRecibos(100); //resto 100 euros a la primera cuenta
$persona1->cuentas[0]->pagarRecibos(100); // resto 100 euros a la primera cuenta
echo (int)$persona1->morosa();
var_dump($persona1);