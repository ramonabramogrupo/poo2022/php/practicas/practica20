<?php

namespace clases\ejercicio2;

class Persona {
    public ?string $dni=null;
    public ?string $nombre=null;
    public ?string $apellidos=null;
    public ?string $direccion=null;
    public array $telefonos=[];
    public array $cuentas=[];
    
    public function __construct(?string $dni=null, ?string $nombre=null, ?string $apellidos=null, ?string $direccion=null, ?string $telefono=null, ?Cuenta $cuenta=null) {
        $this->dni = $dni;
        $this->nombre = $nombre;
        $this->apellidos = $apellidos;
        $this->direccion = $direccion;
        /** comprobar si me han pasado un telefono **/
        if(!is_null($telefono)){
            $this->telefonos[]=$telefono;
        }
        
        /** comprobar si me han pasado una cuenta **/
        if(!is_null($cuenta)){
            $this->cuentas[] = $cuenta;
        }
    }
    
    public function agregarCuenta(Cuenta $cuenta): void {
        if(count($this->cuentas)<3){
            $this->cuentas[]=$cuenta;
        }
    }
    
    public function morosa() : bool{
        foreach ($this->cuentas as $cuenta){
            if($cuenta->getSaldo()<0){
                return true;
            }
        }
        
        return false;
    }
    
    public function getDni(): ?string {
        return $this->dni;
    }

    public function getNombre(): ?string {
        return $this->nombre;
    }

    public function getApellidos(): ?string {
        return $this->apellidos;
    }

    public function getDireccion(): ?string {
        return $this->direccion;
    }

    public function getTelefonos(): array {
        return $this->telefonos;
    }

    public function getCuentas(): array {
        return $this->cuentas;
    }

    public function setDni(?string $dni): void {
        $this->dni = $dni;
    }

    public function setNombre(?string $nombre): void {
        $this->nombre = $nombre;
    }

    public function setApellidos(?string $apellidos): void {
        $this->apellidos = $apellidos;
    }

    public function setDireccion(?string $direccion): void {
        $this->direccion = $direccion;
    }

    public function setTelefonos(array $telefonos): void {
        $this->telefonos = $telefonos;
    }
    
    public function agregarTelefono(string $telefono):void{
        $this->telefono[]=$telefono;
    }




    

    
}
