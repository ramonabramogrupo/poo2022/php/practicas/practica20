<?php

namespace clases\ejercicio2;

class Cuenta {
    public int $numero;
    private float $saldo=0;
    
    public function getSaldo(): float {
        return $this->saldo;
    }
    
    public function recibirAbonos(float $ingreso): void{
        $this->saldo+=$ingreso;
    }
    
    public function pagarRecibos(float $pago):void{
        $this->saldo-=$pago;
    }
    public function __construct() {
        
    }


}
