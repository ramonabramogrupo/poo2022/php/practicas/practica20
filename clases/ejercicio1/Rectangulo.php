<?php
namespace clases\ejercicio1;

class Rectangulo {
   private int $base;
   private int $altura;
   
   public function getBase(): int {
       return $this->base;
   }

   public function getAltura(): int {
       return $this->altura;
   }

   public function setBase(int $base): void {
       $this->base = $base;
   }

   public function setAltura(int $altura): void {
       $this->altura = $altura;
   }

   public function __construct(int $base, int $altura) {
       $this->base = $base;
       $this->altura = $altura;
   }
   
   public function calcularArea() :  int{
       return $this->base * $this->altura;
   }
   
   public function calcularPerimetro() : int{
       return ($this->base+$this->altura)*2;
   }
   
   


}
