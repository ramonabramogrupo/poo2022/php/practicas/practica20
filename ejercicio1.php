<?php
// autocarga de clases
spl_autoload_register(function ($clase) {
    require $clase . '.php';
});

use clases\ejercicio1\Rectangulo;

$r1=new Rectangulo(10, 15);
echo $r1->calcularArea();
echo "<br>";
echo $r1->calcularPerimetro();

$r2=new Rectangulo(45,2);

